mapModule.controller('mapCreateController', ['$scope', 'mapCreate', 'getMapInfo', 'formatter', '$q', '$state',
  function ($scope, mapCreate, getMapInfo, formatter, $q, $state) {

    getMapInfo.getAll().then(function (data) {
      $scope.markers = data.data;
    });

    $scope.filterState = false;

    $scope.toggleState = function () {
      $scope.filterState = !$scope.filterState;
    };

    $scope.peopleState = false;

    $scope.togglePeople = function () {
      $scope.peopleState = !$scope.peopleState;
      $scope.eventState = false;
      $scope.meetingState = false;
    };
    

    $scope.meetingState = false;

    $scope.toggleMeeting = function () {
      $scope.meetingState = !$scope.meetingState;
      $scope.eventState = false;
      $scope.peopleState = false;
    };

    $scope.eventState = false;

    $scope.toggleEvent = function () {
      $scope.meetingState = false;
      $scope.peopleState = false;
      $scope.eventState = !$scope.eventState;
    };

    $scope.cardVisible = false;
    $scope.cardCounter = 0;

    $scope.toggleCard = function () {
      $scope.cardVisible = false;
    };

    $scope.getPreviousCard = function () {
      if ($scope.cardCounter > 0) $scope.cardCounter--;
    };

    $scope.getNextCard = function () {
      if ($scope.cardCounter < $scope.cardInfo.length - 1) $scope.cardCounter++;
    };

    $scope.getFormattedDistance = function (distance) {
      return formatter.getDistance(distance);
    };

    $scope.parseDate = function (date) {
      return formatter.formatDate(date);
    };

    $scope.coordinates = new Map();

    $scope.getAddress = function (lat, lng) {
      return formatter.getAddress(lat, lng);
    };

    $scope.$watchGroup([
        function () {
          return mapCreate.cardsArray
        },
        function () {
          return mapCreate.coordinatesMap
        }
      ],
      function (newVal, oldVal) {

        if (newVal[0] != 'undefined' && newVal[0] != oldVal[0]) {
          $scope.cardVisible = true;
          $scope.cardInfo = mapCreate.cardsArray;
          $scope.cardCounter = 0;
        }

        if (newVal[1] != 'undefined') {
          $scope.coordinates = mapCreate.coordinatesMap;
        }

      });

    $scope.zoomIn = function () {
      mapCreate.zoomIn();
    };

    $scope.zoomOut = function () {
      mapCreate.zoomOut();
    };

    $scope.filterMap = function () {

      let options = {
        'meetingStartAtFrom': formatter.getUnixTime($scope.meetingTimeFrom),
        'meetingStartAtTo': formatter.getUnixTime($scope.meetingTimeTo),
        'meetingMembersCountFrom': $scope.meetingMembersFrom,
        'meetingMembersCountTo': $scope.meetingMembersTo,
        'eventStartAtFrom': formatter.getUnixTime($scope.eventTimeFrom),
        'eventStartAtTo': formatter.getUnixTime($scope.eventTimeTo),
        'eventMembersCountFrom': $scope.eventMembersFrom,
        'eventMembersCountTo': $scope.eventMembersTo,
        'userGender': $scope.getGender($scope.genderMale, $scope.genderFemale),
        'userAgeFrom': $scope.ageFrom,
        'userAgeTo': $scope.ageTo
      };

      getMapInfo.getFilteredInfo(options).then(function (data) {
        mapCreate.clearMap();
        $scope.markers = data.data;
      });

    };

    
    $scope.centerMap = function () {
      mapCreate.centerMapToUser();
    };

    $scope.getGender = function (male, female) {
      return formatter.getGender(male, female);
    };
  }]);